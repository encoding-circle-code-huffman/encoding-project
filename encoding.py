from sage.misc.prandom import randint
import sys, base64, json
from heapq import heappush, heappop, heapify
from collections import defaultdict
from sage.all import *

class sender:
    def fileIo(self):
        #gets the txt as a parameter in the program
        fo = open(str(sys.argv[1]), "r")
        sth = fo.read()
        fo.close()
        return sth

    def createStats(self):
        sth = self.fileIo()
        s = defaultdict(int)
        for ch in sth:
            s[ch] += 1
        return s

    def compression(self, s):
        #Huffman encoding
        heap = [[fr, [sym, ""]] for sym, fr in s.items()]
        heapify(heap)
        while len(heap) > 1:
            left = heappop(heap)
            right = heappop(heap)
            for i in left[1:]:
                i[1] = '0' + i[1]
            for i in right[1:]:
                i[1] = '1' + i[1]
            #print(heap)
            heappush(heap, [left[0] + right[0]] + left[1:] + right[1:])
        return sorted(heappop(heap)[1:], key=lambda p: (len(p[-1]), p))

    def createBinarycompression(self, text, list):
        totalBin = ""
        #itterate through string
        for ch in text:
            #itterate through list
            for i in list:
                #check character
                if i[0] == ch:
                    totalBin += i[1]        
        return totalBin

    def encodeBase64(self, str):
        return base64.b64encode(str)

    def convertToPolynomial(self, vector):
        polynomial = []
        for i in range(len(vector)):
            if(vector[i] == "1"):
                polynomial.append("x**" + str(len(vector)-(i+1)))
            else:
                polynomial.append("0")
        return " + ".join(polynomial)

    def cyclic_encode(self, msg, n, g):
        #SAGE CODE FOR ERROR CORRECTION AND ENCODING ###################
        R = PolynomialRing(GF(2), "x") 
        x = R.gens()[0]
        gen = R(g)
        c = R(msg)
        E = codes.CyclicCode(generator_pol = gen, length = n)
        C = codes.encoders.CyclicCodePolynomialEncoder(E)
        s = C.encode(c)
        return s

    def add_noise(self, plithos, bitlist, msgl):
    	added = []
    	for i in range(plithos):
    		ns = randint(0, len(bitlist)-1)
    		while ns in added:
    			ns = randint(0, len(bitlist)-1)
    		added.append(ns)
    		tmp = list(bitlist[ns])
    		noise_bit = randint(0, len(tmp)-1)
    		if tmp[noise_bit] == "1":
    			tmp[noise_bit] = "0"
    		else:
    			tmp[noise_bit] = "1"
    		bitlist[ns] = "".join(tmp)
    	return bitlist

    def json_write(self, l, gen_pol, sd, enc_string, msgl, nbits, lastlen):
    	data = {
    			"compression_algorithm" : { 
	    			"name" : "huffman",
	    			"symbol_dictionary" : sd
    			},
    			"code" : {
	    			"name" : "cyclic code",
	    			"length" : l,
	    			"generator_polynomial" : gen_pol,
	    			"message_length" : msgl,
	    			"noise_bits" : nbits,
	    			"last" : lastlen
    			},
    			"encoded_data" : enc_string
    	}
    	f = open('encoding_data.json', 'w')
    	json.dump(data, f)

    def crc(self, l, gp, msgl, nb):
        s = self.createStats()
        huffmandict = self.compression(s)
        huffmanstring = self.createBinarycompression(self.fileIo(), huffmandict)
        splitted = string_split(huffmanstring, msgl)
        crc = []
        for i in splitted:
            polyn = self.convertToPolynomial(i)
            t = str(self.cyclic_encode(polyn, l, gp))
            t = t.replace(", ", "")
            t = t.replace("(", "")
            t = t.replace(")", "")
            crc.append(t)
        crc = self.add_noise(nb, crc, msgl)
        crc_str = "".join(crc)
        crc_str = self.encodeBase64(crc_str)
        self.json_write(l, gp, huffmandict, crc_str, msgl, nb, len(splitted[-1]))
        return crc_str


class receiver:
    def decompression(self, text, list):
        result = ""
        #Itterate through text
        while text:
            #itterate through list
            for i in list:
                #if binary text starts with any assign code
                if text.startswith(i[1]):
                    #add letter to result
                    result += i[0];
                    #remove binary from given text
                    text = text[len(i[1]):];
        return result

    def decodeBase64(self, base64EncodedString):
        return base64.b64decode(base64EncodedString)

    def cyclic_decode(self, wmsg, n, g, length):
        R = PolynomialRing(GF(2), "x") 
        x = R.gens()[0]
        gen = R(str(g))
        E = codes.CyclicCode(generator_pol = gen, length = n)
        C = codes.encoders.CyclicCodePolynomialEncoder(E)
        s = vector(GF(2), wmsg)
        c_dec = E.decode_to_code(s)
        m_unenc = C.unencode(c_dec)
        m = vector(GF(2), m_unenc)
        m = str(m)
        m = m.replace(", ", "")
        m = m.replace("(", "")
        m = m.replace(")", "")
        if len(m) < length:
        	m += (length - len(m))*"0"
        m = m[::-1]
        return m

    def json_read(self):
    	f = open('encoding_data.json', 'r')
    	return json.load(f)

    def full_decode(self):
   		jf = self.json_read()
   		sd = jf["compression_algorithm"]["symbol_dictionary"]
   		n = jf["code"]["length"]
   		g = jf["code"]["generator_polynomial"]
   		msgl = jf["code"]["message_length"]
   		last = jf["code"]["last"]
   		data = jf["encoded_data"]
   		data = self.decodeBase64(data)
   		datalist = string_split(data, n)
   		lastel = datalist.pop()
   		decoded = ""
   		for i in datalist:
   			decoded += self.cyclic_decode(i, n, g , msgl)
   		decoded += self.cyclic_decode(lastel, n, g, last)
   		decompressed = self.decompression(decoded, sd)
   		return decompressed



    	
def string_split(line, n):
        return [line[i:i+n] for i in range(0, len(line), n)]

#Calculated the entropy of a binary
def entropy(binString):
    # get probability of chars in string
    probability = [ float(binString.count(i)) / len(binString) for i in dict.fromkeys(list(binString)) ]

    # calculate the entropy
    entropy = - sum([ p * math.log(p) / math.log(2.0) for p in probability ])

    return entropy

def main():
    sen = sender()
    l = input("Give the length of the Cyclic code: ")
    gp = raw_input("Give the generator polynomial of the Cyclic code (Example: x**3 + x + 1): ")
    msgl = input("Give the length of the message (Note: It should be less/equal to n - k, where n = length of code and k = generator polynomial's degree): ")
    nb = input("Give the number of noise bits: ")
    enc_data = sen.crc(l, gp, msgl, nb)
    print("Encoded data: " + enc_data)
    print("Old entropy: " + str(entropy(sen.fileIo())))
    print("New entropy: " + str(entropy(sen.createBinarycompression(sen.fileIo(),sen.compression(sen.createStats())))))
    print("Old data size: " + str(len(sen.fileIo())) + " bytes")
    print("New data size: " + str(len(sen.createBinarycompression(sen.fileIo(),sen.compression(sen.createStats())))/8) + " bytes")
    rec = receiver()
    dec_data = rec.full_decode()
    print("Decoded data: " + dec_data)

if __name__== "__main__":
    main()
